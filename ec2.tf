#Generic provisioners (file,remote-exec,local-exec)

#Below is for type: file i.e by using this we can transfer any file,folder from local machine to aws ec2 instance.
#configure the aws provider
provider "aws" {
    region = "us-east-1"
}

#Terraform Block
terraform {
  #required_version = "0.14.10"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.36.0"
    }
  }
}

#Simple data module for getting current external IP address
module "myip" {
  source  = "4ops/myip/http"
  version = "1.0.0"
}

#EC2 Instance
resource "aws_instance" "EC2instance" {
  ami             = "ami-09e67e426f25ce0d7"
  instance_type   = "t2.micro"
  key_name        = "ec2server"
  vpc_security_group_ids = [aws_security_group.SSHRule.id]
  tags = {
    Name = "Task-ec2"
  }

  #provisioner type: file
  provisioner "file" {
      source = "E:/Training_LS/tf_provisioners_example/test.txt"
      destination = "/home/ubuntu/test.txt" 
  }
  connection {
      type = "ssh"
      host = self.public_ip
      user = "ubuntu"
      private_key = file("E:/Training_LS/tf_provisioners_example/ec2server")
      timeout = "4m"
  }

  #local-exec provisioner
  provisioner "local-exec" {
      command = "echo hello local exec > hello_local_exec.txt"
  }

  #remote-exec provisioner
  provisioner "remote-exec" {
      inline = [
          "touch hello.txt",
          "echo hello remote provisioner >> hello.txt"
      ]

  }

}

#KeyPair
resource "aws_key_pair" "owner" {
  key_name   = "ec2server"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDjrERf4je4L7YpaVSzsoPUs0EaFJFe+lkBudjthRhfJm0CdS3LAMY5h2FG0cvNKQQfhI76dvc56AP3tJqk6oaifkpTCSnNLYxkD+yMQDWRj1xvGurq8OfGBgh8Qj200E8Bzbw14I523eysGPscrZEPduBaSx7Yen6FTr2ndh93nFlsn55U8EnckKAbF73ZbBxhAswceXw822RX0RpD2iGlnGt3DcX87lccCDXcj2/V2B0M5k0GbB+726Lpxd/YoTCam5VbupEtPlXTW8fLZUe2PgATsXx56PiwEIWXlwoPw53xyaJP8vA2SiIS0GkvtiNlE516vOnQB68ssRQXUfRt prajwal@DESKTOP-J088C6I"

}

#Security Group
resource "aws_security_group" "SSHRule" {
  name = "EC2_SSH"

  ingress {
    description = "Allow SSH inbound connection"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = formatlist("%s/32", [module.myip.address])
  }
  ingress {
    description = "Allow HTTP inbound connection"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow HTTPS inbound connection"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "All Traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}



